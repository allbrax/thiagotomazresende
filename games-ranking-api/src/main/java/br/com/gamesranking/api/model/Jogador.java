package br.com.gamesranking.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "JOGADOR")
public class Jogador implements Comparable<Jogador> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "NOME", nullable = false, length = 20)
	private String jogador;

	@Column(name = "QTD_VITORIAS")
	private int vitorias;

	@Column(name = "QTD_PARTIDAS")
	private int partidas;

	public Jogador() {
	}

	public Jogador(String jogador, int vitorias, int partidas) {
		super();
		this.jogador = jogador;
		this.vitorias = vitorias;
		this.partidas = partidas;
	}

	public boolean isSalvo(String jogador) {

		if (this.jogador.equals(jogador))
			return true;
		else
			return false;
	}

	public void adicionarVitorias() {
		this.vitorias += 1;
		this.partidas += 1;
	}

	public void adicionarPartidas() {
		this.partidas += 1;
		this.vitorias += 1;
	}

	public Long getId() {
		return id;
	}

	public String getJogador() {
		return jogador;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setJogador(String jogador) {
		this.jogador = jogador;
	}

	public int getVitorias() {
		return vitorias;
	}

	public int getPartidas() {
		return partidas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Jogador other = (Jogador) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public int compareTo(Jogador outroJogador) {

		if (this.vitorias > outroJogador.getVitorias()) {
			return -1;
		}
		if (this.vitorias < outroJogador.getVitorias()) {
			return 1;
		}
		return 0;
	}

}
