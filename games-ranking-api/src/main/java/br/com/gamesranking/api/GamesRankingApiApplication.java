package br.com.gamesranking.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GamesRankingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GamesRankingApiApplication.class, args);
	}

}

