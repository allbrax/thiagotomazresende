package br.com.gamesranking.api.camadas.resources;


import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.gamesranking.api.camadas.GamesRankingApplicationTests;
import br.com.gamesranking.api.model.Jogador;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
public class JogadorResourceTest extends GamesRankingApplicationTests {

	@Test
	public void deve_salvar_novo_jogador() throws Exception {

		final Jogador jogador = new Jogador("JAIME", 30, 30);

		given()
		.request().header("Accept", ContentType.ANY)
		.header("Content-type", ContentType.JSON)
		.body(jogador)
				.when().post("/api/ranking")
				.then()
				.log().headers()
				.and()
				.log().body()
				.and()
				.statusCode(HttpStatus.CREATED.value())
				.body("jogador", equalTo("JAIME"),
						"vitorias",  equalTo(30),
						"partidas", equalTo(30));
	}


	@Test
	public void nao_deve_salvar_dois_jogadores_com_mesmo_nome() throws Exception {
		
		final Jogador jogador = new Jogador("JOSÉ", 30, 30);

		given()
		.request().header("Accept", ContentType.ANY)
		.header("Content-type", ContentType.JSON)
		.body(jogador)
				.when().post("/api/ranking")
				.then()
				.log().headers()
				.and()
				.log().body()
				.and()
				.statusCode(HttpStatus.NOT_FOUND.value())
				.body("erro", equalTo("Jogador já existe cadastrado com o nome 'JOSÉ'"));

	}
	
	@Test 
	public void deve_listar_os_jogadores_ordenados_por_vitorias() throws Exception {
		
		given()
		.get("/api/ranking/lista")
		.then() 
		.log().body().and()
		.statusCode(200);

	}
	
	@Test
	public void deve_adicionar_partida_para_jogador() throws Exception{
		
		final Jogador jogador = new Jogador("JOSÉ", 30, 30);
		
		given()
		.request().header("Accept", ContentType.ANY)
		.header("Content-type", ContentType.JSON)
		.body(jogador)
				.when().put("/api/ranking/adicionarpartida")
				.then()
				.log().headers()
				.and()
				.log().body()
				.and()
				.statusCode(200);
		
	}
	
	@Test
	public void deve_adicionar_vitoria_para_jogador() throws Exception{
		
		final Jogador jogador = new Jogador("JOSÉ", 30, 30);
		
		given()
		.request().header("Accept", ContentType.ANY)
		.header("Content-type", ContentType.JSON)
		.body(jogador)
				.when().put("/api/ranking/adicionarvitoria")
				.then()
				.log().headers()
				.and()
				.log().body()
				.and()
				.statusCode(200);
		
	}
	
}
