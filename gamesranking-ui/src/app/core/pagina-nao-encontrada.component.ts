import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pagina-nao-encontrada',
  styles: [`img {
    display: block;
    margin-left: auto;
    margin-right: auto;
  }
  h2 { text-align: center }
  `],
  template: `
    <div class="container-fluid">
      <h2>Desculpe, não encontrado o conteúdo relacionado!</h2>
      <img Width="65%" src="http://www.acioleinformatica.com.br/themes/aciole/img/404.png">
    </div>`
})
export class PaginaNaoEncontradaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
