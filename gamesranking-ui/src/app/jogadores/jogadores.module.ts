import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { InputMaskModule } from 'primeng/components/inputmask/inputmask';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { ButtonModule } from 'primeng/components/button/button';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { TooltipModule } from 'primeng/components/tooltip/tooltip';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { RadioButtonModule } from 'primeng/components/radiobutton/radiobutton';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { PanelModule } from 'primeng/components/panel/panel';

import { CurrencyMaskModule } from 'ng2-currency-mask';

import { JogadoresPesquisaComponent } from './jogadores-pesquisa/jogadores-pesquisa.component'
import { JogadorCadastroComponent } from './jogador-cadastro/jogador-cadastro.component'
import { AppRoutingModule } from '.././app.routing';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    InputTextModule,
    ButtonModule,
    DataTableModule,
    TooltipModule,
    InputMaskModule,
    DropdownModule,
    RadioButtonModule,
    CalendarModule,
    CurrencyMaskModule,
    PanelModule,
    AppRoutingModule,
    SharedModule
  ],
  declarations: [
    JogadoresPesquisaComponent,
    JogadorCadastroComponent],
  exports: []
})

export class JogadoresModule { }
