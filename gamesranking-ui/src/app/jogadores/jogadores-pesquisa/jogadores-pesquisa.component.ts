import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { ToastyService } from 'ng2-toasty';

import { ConfirmationService } from 'primeng/components/common/api';
import { ErrorHandlerService } from '../../core/error-handler.service';
import { JogadoresService, JogadorFiltro } from './../jogadores.service'

@Component({
  selector: 'app-jogadores-pesquisa',
  templateUrl: './jogadores-pesquisa.component.html',
  styleUrls: ['./jogadores-pesquisa.component.css']
})
export class JogadoresPesquisaComponent implements OnInit {

  filtro = new JogadorFiltro();
  jogadores = [];
  @ViewChild('tabela') grid;

  constructor(private title: Title,
    private toasty: ToastyService,
    private confirmation: ConfirmationService,
    private errorHandler: ErrorHandlerService,
    private jogadorService: JogadoresService
  ) { }

  ngOnInit() {
    this.title.setTitle('Jogadores - Visualiza Ranking');
    this.pesquisar();
  }

  pesquisar() {

    this.jogadorService.listaJogadoresOrdemVitorias()
      .then(resultado => {
        this.jogadores = resultado;
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  confirmarAdicionarVitoria(jogador: any) {

    this.confirmation.confirm({
      message: 'Tem certeza que deseja adicionar Vitória',
      accept: () => {
        this.adicionarVitoria(jogador);
      }
    });
  }

  adicionarVitoria(jogador: any) {

    this.jogadorService.adicionaVitoria(jogador)
      .then(() => {
        if (this.grid.first === 0) {
          this.pesquisar();
        } else {
          this.grid.first = 0;
        }
        this.toasty.success('Vitória registrada com sucesso!');
      })
  }

  confirmarAdicionarPartida(jogador: any) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja adicionar partida?',
      accept: () => {
        this.adicionarPartida(jogador);
      }
    });
  }

  adicionarPartida(jogador: any) {
    this.jogadorService.adicionaPartida(jogador)
      .then(() => {

        if (this.grid.first === 0) {
          this.pesquisar();
        } else {
          this.grid.first = 0;
        }
        this.toasty.success('Partida registrada com sucesso!');
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

}
